Task: Players
Description: Packages which makes your multimedia tools complete
 This metapackage will install additional 'consumer' multimedia software.
Install: true

Depends: vlc

Depends: vlc-plugin-jack

Depends: moc

Depends: smplayer

Suggests: aqualung
Remark: Package removed from testing & sid as incompatible with libav

Depends: audacious

Suggests: audacious-plugins

Depends: cantata

Depends: cmus

Suggests: easytag, easytag-nautilus

Depends: flactag

Depends: freetuxtv

Depends: gecko-mediaplayer

Recommends: glyrc

Depends: gmerlin

Depends: gmerlin-plugins-avdecoder

Depends: gmerlin-encoders-extra, gmerlin-encoders-ffmpeg, gmerlin-encoders-good

Depends: gmusicbrowser

Depends: gnome-mplayer

Depends: gpac

Depends: guayadeque

Depends: isrcsubmit

Depends: mjpegtools

Depends: mp3fs

Recommends: mp4v2-utils

Depends: mpd-sima

Depends: mpeg2dec

Depends: mpg123

Depends: browser-plugin-vlc

Recommends: opencubicplayer

Depends: showq

Recommends: silan

Depends: smplayer-themes

Depends: smtube

Depends: streamtuner2

Depends: videolan-doc

Depends: groovebasin

Suggests: kodi

Suggests: vdr-plugin-vnsiserver

Suggests: audiotools

Depends: deadbeef
Pkg-Description: Ultimate Music Player For GNU/Linux
 DeaDBeeF (as in 0xDEADBEEF) is an audio player for GNU/Linux with X11,
 and it also support command line.
Homepage: http://deadbeef.sourceforge.net/
License: GPL and LGPL Version 2
WNPP: 576975

Depends: gstreamer-sharp-1.0
Pkg-Description: New GI-based bindings to GStreamer 1.x
 GStreamer-Sharp are C# bindings to GStreamer. This version of the bindings
 has been written from scratch, mainly by Stephan Sundermann in GSoC, based
 on bindinator (a new GObject-Introspection-based code-generation approach).
 This version of the bindings is a "preview" (0.99.0) which means it will be
 API-unstable until its final version (1.0), and it targets GStreamer 1.0
 API. Soname of the bound library is libgstreamer-1.0.so.0.
 .
 Banshee v2.9.1 is the first version which consumes this binding, for
 example.
WNPP: 742729

Depends: libgstreamer-java
Pkg-Description: a set of java bindings for the gstreamer framework.
 In spite of being described by upstream authors as
 unofficial/alternative, gstreamer-java is currently the java binding
 available at gstreamer language bindings  page [1].
 [1] http://gstreamer.freedesktop.org/bindings/
License: LGPL v3.
WNPP: 576984

Depends: mediagoblin

Depends: faad

Depends: beatbox
Pkg-Description: A music player written in vala
 A music player with focus on speed, simplicity and music discovery.
Homepage: https://launchpad.net/beat-box
License: GNU GPL v3
WNPP: 617617

Depends: bs1770gain

Depends: cloudruby
Pkg-Description: Ncurses player for Soundcloud tracks
 A soundcloud player written in Ruby with Ncurses for graphical
 interface and mpg123 for playback.
Homepage: https://github.com/kulpae/cloudruby
License: MIT
WNPP: 726324

Depends: despotify
Pkg-Description: Open source client for Spotify
 Despotify is an independent open source client for the Spotify streaming music
 platform, which provides on-demand access to music from a number of major and
 independent record labels. To use despotify, a Spotify Premium account is
 required.
 .
 The source package builds three binaries:
 libdespotify0, libdespotify0-dev, and despotify.
 .
 The binary package "despotify" provides two console front-ends, and one HTTP
 REST front-end. The two library packages are self-explanatory. I intend to
 investigate also building the python and ruby bindings, but I have not yet
 looked into that.
 .
 For answers about the legal situation and the relationship of despotify with
 the official proprietary distribution, please see <http://despotify.se/faq/>.
Homepage: http://despotify.se/
License: 2-clause BSD
WNPP: 670786

Depends: eina-player
Pkg-Description: Simple music player based on gtk3 and gstreamer
 Eina (or eina-player for avoid collision with libeina from e17) is a 
 simple and extensible music player.
 .
 Current music players are focused on managing your music collection, 
 while using mandatory criteria about how they organize it.
 .
 Eina does not care about it at all. If your are an organized person, 
 chances are your music collection is organized too, and you won't like 
 to spend your time rearranging it to follow your player's specific 
 rules. In addition, having those players to arrange music that's 
 randomly stored, named or tagged, is usually counterproductive.
 .
 - Based on gtk+ 3 and gstreamer 0.10.
 - Extensible via plugins, next version will support python and 
 javascript plugins thanks to libpeas.
 - Small memory footprint.
 - System integration with support for notifications, XDG-compilant, 
 stock icons.
 - Follows as close as possible Gnome HIG guidelines.
 .
 Eina developer, me, is totally open to collaboration to get Eina into 
 Debian, so ask or report anything.
Homepage: http://eina.sourceforge.net/
WNPP: 631356

Depends: flac123
Pkg-Description: command-line program for playing FLAC audio files
Homepage: http://flac-tools.sourceforge.net/
License: GPL v2
WNPP: 759198

Depends: flash-player-mp3
Pkg-Description: MP3 flash player for websites
 flash-player-mp3 is an audio player for MP3 file, intended for diffusion
 on websites. It's highly customizable.
Homepage: http://code.google.com/p/mp3player/
License: MPL 1.1
WNPP: 609107

Depends: flashplugin-nonfree-pulse
Pkg-Description: Non free Flash plugin for Pulse Audio
Homepage: http://pulseaudio.vdbonline.net/
WNPP: 575645

Depends: foobnix
Pkg-Description: Simple and Powerful music player for Linux
 All best features in one player. Foobnix small, 
 fast, customizable, powerful music player with 
 user-friendly interface.
 .
 Main features player:
 * Support for CUE (also wv, iso.wv) is the best under Linux (zavlab)
 * Formats MP3, MP4, AAC, CD Audio, WMA, Vorbis, FLAC, WavPack, WAV, AIFF, Musepack, Speex, AU, SND ...
 * Converter any format to any (mp3, ogg, mp2, ac3, m4a, wav)
 * Scrobbler tags with music and radio
 * Find and play music and videos
 * Equalizer
 * Online music download manager
 * Shortcuts
 * Displays the album cover, lyrics, photo artist
 * Integration with VKontakte (displaying all the friends and their music, downloading music from the group vkontakte)
 * Integration with Last.FM (Show plays the best songs, favorite songs, artists)
Homepage: http://www.foobnix.com/
License: GPL
WNPP: 653159

Depends: fuoco-converter
Pkg-Description: Universal audio and video converter
 Fueco is usefull to understand ffmpeg and mencoder. It can convert to .ogg, to
 .wav, to ac3 video, etc...
Homepage: http://fuocotools.byethost13.com/index.php
License: GPL
WNPP: 489783

Suggests: genivi-audio-manager
Pkg-Description: software framework for low-level car audio management
 The AudioManager is meant to abstract audio in an IVI setting in a way that creates
 a common API allowing an independence from routing mechanisms. Designed to handle
 both dynamic and static sources and sinks it provides an API upwards to other
 applications, like HMI. The AudioManager is not another routing mechanism however.
Homepage: http://www.genivi.org/projects
License: Mozilla Public License v2.0
WNPP: 688627

Suggests: gmpc-plugin-coveramazon
Pkg-Description: Download cover art and album information from the amazon web service
 The Coveramazon plugin is a a metadata provider for gmpc which
 downloads cover art and album information.
Homepage: http://gmpc.wikia.com/wiki/GMPC_PLUGIN_COVERAMAZON
License: GPL
WNPP: 524990

Suggests: ha-audacious
Pkg-Description: player for GameBoy Advanced(R) chiptunes
 ha-audacious is a port of the "Highly Advanced" Winamp chiptune plugin
 to Audacious. It can play GSF and MiniGSF format chiptunes through
 emulation for the highest amount of possible accuracy.
Homepage: http://nenolod.net/audacious
License: GPL
WNPP: 466187

Depends: jampal
Pkg-Description: mp3 song library management system and player
Homepage: http://jampal.sourceforge.net/
License: GPL
WNPP: 690274

Recommends: lastfm-desktop
Pkg-Description: The official Last.fm desktop application suite
 Last.fm's collection of applications for streaming and scrobbling
 music.
Homepage: http://github.com/jonocole/lastfm-desktop/
License: GPL3+
WNPP: 537200

Recommends: laudio
Pkg-Description: HTML5 music player
 Laudio is an HTML5 Music Player which can be accessed by anyone, from
 anywhere with a webbrowser and internet connection.  Laudio is easy to use
 simply add a music directory, scan music directory and enjoy.
 .
 User accounts can be added to restrict usage to registered users.
Homepage: https://github.com/Raydiation/Laudio
License: GPL-3
WNPP: 614899

Suggests: mpcut
Pkg-Description: Visual MP3 editor (lossless)
 A graphical editor that allows you to replay or
 remove selected parts (frames) of an MP3 file.
Homepage: http://minnie.tuhs.org/Programs/Mpcut
License: BSD
WNPP: 437954

Recommends: mpdas

Recommends: mpx
Pkg-Description: library-oriented media player
 MPX is a media player which provides a very easy-to-use interface and usage
 semantics for all tasks, while having extensive standards and services support
 under the hood (MusicBrainz, Last.fm radio/scrobbling, HAL, DBus),
 yet keeping the details out of the way of the user.
 .
 MPX is a media player that features support for specifications
 like XDS DnD, XSPF and DBus. MPX is highly interoperable and integrates well
 with other applications and a variety of desktop environments.
 .
 MPX guys are writing a successor to MPX now called mpx. I intend to
 package it once it becomes usable. So, the same long description as
 MPX will mostly apply here, as it's basically the same idea, but with
 newer code.
Homepage: http://hg.backtrace.info/mpx
License: GPL
WNPP: 471726

Recommends: mtpsync
Pkg-Description: Syncronize files/music with a MTP device
 MTPSync is used to syncronize files/music with a MTP device (as supported
 by libmtp)
 .
 Feature:
  * Graphical or console interface
  * Syncronising Music
  * Syncronising Playlists (local playlists stored in m3u format)
  * Syncronising Videos
  * Syncronising Zencasts
  * Syncronising Organiser (calendar/contacts/tasks)
  * Syncronising Pictures
 .
 NOTE: It already have a debian package avilable for download on the home page.
Homepage: http://www.adebenham.com/mtpsync/
License: GPL
WNPP: 494738

Suggests: mudkip-player
Pkg-Description: an XMMS replacement
 Mudkip-player is a combination of the Audacious 1.5 and BMPx
 (old XMMS-like version) codebases. It behaves like XMMS and includes
 a few useful enhancements. Mudkip is based on GStreamer and is not
 compatible with XMMS plugins, but includes select features from both
 BMPx and Audacious.
Homepage: http://www.nenolod.net/mudkip-player
License: GPL
WNPP: 480509

Depends: mopidy-local-sqlite

Depends: mopidy-podcast

Depends: mopidy-podcast-itunes

Depends: mopidy-mpd

Depends: mopidy-local

Depends: nightingale
Pkg-Description: audio player and web browser based on the Songbird
 A good substitute for songbird.
Homepage: http://getnightingale.com/
License: GPL
WNPP: 712631

Depends: sayonara

Suggests: osd-lyrics
Pkg-Description: An OSD lyric show compatible with various media players
 OSD Lyrics is inspired by lrcdis. It is a third-party lyrics
 display program, and focus on OSD lyrics display.
 OSD Lyrics currently supports following media players:
 * Amarok 2.0 and 1.4 (1.4 is disabled by default, use --enable-amarok1 to compile)
 * Audacious
 * Banshee
 * Exaile (Both 0.2 and 0.3)
 * JuK
 * MOC 2.5 (High CPU Usage)
 * Muine new!
 * Qmmp (with MPRIS plugin enabled)
 * Quod Libet
 * MPD
 * Rhythmbox
 * Songbird (with MPRIS extension installed)
 * XMMS2 
 OSD Lyrics can download lyrics from following web sites:
 * Sogou
 * Qianqian
 * MiniLyrics
Homepage: http://code.google.com/p/osd-lyrics/
License: GPL
WNPP: 594051

Depends: panucci
Pkg-Description: Resuming audioplayer
 Panucci is an ideal player for audiobooks and
 podcasts. It has an resuming function to
 continue at the last timestamp. It interacts
 well with gpodder.
Homepage: http://gpodder.org/panucci/
License: GPLv3
WNPP: 594794

Suggests: phonon-backend-mplayer
Pkg-Description: mplayer backend for phonon
 This package should provide the mplayer backend for phonon, like the
 gstreamer and the xine ones.
Homepage: http://websvn.kde.org/trunk/playground/multimedia/phonon-backends/mplayer/
License: GPL
WNPP: 572788

Suggests: playdar
Pkg-Description: music content resolver
 Playdar is a music content resolver service - run it on every computer
 you use, and you'll be able to listen to all the songs you would
 otherwise be able to find manually by searching though all your
 computers, hard disks, online services, and more.
Homepage: http://www.playdar.org/
License: MIT/X
WNPP: 552120

Suggests: pogo
Pkg-Description: Probably the simplest and fastest audio player for Linux
 Pogo plays your music. Nothing else. It is both fast and easy-to-use. 
 The clear interface uses the screen real-estate very efficiently. Other
 features include:
 Fast search on the harddrive and in the playlist, smart album grouping, 
 cover display, desktop notifications and no music library.
 Pogo is a fork of Decibel Audio Player and supports most common audio 
 formats.
 It is written in Python and uses GTK+ and gstreamer.
Homepage: http://launchpad.net/pogo
License: GPL2+
WNPP: 666008

Suggests: pragha

Suggests: pythm
Pkg-Description: media players (e.g. mpd, gstreamer, mplayer) GUI frontend
 Pythm (Python + Rhythm) is a media player frontend, designed to control
 gstreamer, mplayer or mpd with one GUI on mobile devices such as
 OpenMoko's FreeRunner.
Homepage: http://github.com/negi/pythm/tree/master
License: GPL-2+
WNPP: 518494

Depends: mopidy-somafm

Depends: mopidy-internetarchive

Depends: castnow
Pkg-Description: command-line chromecast player
 Castnow is a command-line utility that can be used to play back media
 files on your Chromecast device. It supports playback of local video
 files, YouTube clips, videos on the web and torrents. You can also
 re-attach a running playback session.
Homepage: https://github.com/xat/castnow
License: MIT
WNPP: 813736

Depends: baka-mplayer
Pkg-Description: A libmpv based media player
 Baka MPlayer is a free and open source, cross-platform, libmpv based multimedia
 player. Its simple design reflects the idea for an uncluttered and enjoyable
 environment for watching tv shows. Features:
 .
 * Gesture seeking.
 * Smart playlist.
 * Dim Desktop.
 * Hardware accelerated playback (vdpau, vaapi, vda).
 * Youtube playback support (and others).
 * Multilingual support (we are looking for translators!).
 * And more...
Homepage: http://bakamplayer.u8sand.net/
License: GPL-2
WNPP: 813591
Vcs-Git: https://anonscm.debian.org/git/pkg-multimedia/baka-mplayer.git
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-multimedia/baka-mplayer.git

Suggests: xplayer
Pkg-Description: Simple media player based on GStreamer
 Xplayer is part of X-App project, is a simple yet featureful media player
 which can read a large number of file formats. It features :
 .
    * Shoutcast, m3u, asx, SMIL and ra playlists support
    * DVD (with menus), VCD and Digital CD (with CDDB) playback
    * TV-Out configuration with optional resolution switching
    * 4.0, 5.0, 5.1 and stereo audio output
    * Full-screen mode (move your mouse and you get nice controls) with
      Xinerama, dual-head and RandR support
    * Aspect ratio toggling, scaling based on the video's original size
    * Full keyboard control
    * Simple playlist with repeat mode and saving feature
    * GIO integration
    * Screenshot of the current movie
    * Brightness and Contrast control
    * Visualisation plugin when playing audio-only files
    * Video thumbnailer
    * Works on remote displays
    * DVD, VCD and OGG/OGM subtitles with automatic language selection
    * Extensible with plugins
 .
 This package provides uniformity across the XFCE, Mate & LXDE desktpos
 often preferred on older computers.
Homepage: https://github.com/linuxmint/xplayer
License: GPL, LGPL
WNPP: 830624

Depends: parlatype

Suggests: miniaudio
Homepage: https://github.com/dr-soft/miniaudio
License: MIT or Unlicense
Pkg-Description: single file library for audio playback and capture
 Miniaudio (formerly mini_al) is a single file library for audio playback and
 capture. It's written in C (compilable as C++) and released into the public
 domain.
WNPP: 950181
